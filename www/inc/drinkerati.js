function Drinkerati() {
	that = this;
}

Drinkerati.prototype.setup = function(callback) {

	//First, setup the database
	this.db = window.openDatabase("drinkerati", 1, "drinkerati", 1000000);
	this.db.transaction(this.initDB, this.dbErrorHandler, callback);

}

//Geenric database error handler. Won't do anything for now.
Drinkerati.prototype.dbErrorHandler = function(e) {
	console.log('DB Error');
	console.dir(e);
}

//I initialize the database structure
Drinkerati.prototype.initDB = function(t) {
	t.executeSql('create table if not exists drinkerati(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, image TEXT, balance INTEGER)');
}

Drinkerati.prototype.getEntries = function(start,callback) {
	console.log('Running getEntries');
	if(arguments.length === 1) callback = arguments[0];

	this.db.transaction(
		function(t) {
			t.executeSql('select id, name, image, balance from drinkerati order by name desc',[],
				function(t,results) {
					callback(that.fixResults(results));
				},this.dbErrorHandler);
		}, this.dbErrorHandler);

}

Drinkerati.prototype.getEntry = function(id, callback) {

	this.db.transaction(
		function(t) {
			t.executeSql('select id, name, image, balance from drinkerati where id = ?', [id],
				function(t, results) {
					callback(that.fixResult(results));
				}, this.dbErrorHandler);
			}, this.dbErrorHandler);

}

//No support for edits yet
Drinkerati.prototype.saveEntry = function(data, callback) {
console.dir(data);
	this.db.transaction(
		function(t) {
			t.executeSql('insert into drinkerati(title,image,balance) values(?,?,?)', [data.name, "http://placehold.it/210x210/ffffff/999999", 0],
			function() { 
				callback();
			}, this.dbErrorHandler);
		}, this.dbErrorHandler);
}

//Utility to convert record sets into array of obs
Drinkerati.prototype.fixResults = function(res) {
	var result = [];
	for(var i=0, len=res.rows.length; i<len; i++) {
		var row = res.rows.item(i);
		result.push(row);
	}
	return result;
}

//I'm a lot like fixResults, but I'm only used in the context of expecting one row, so I return an ob, not an array
Drinkerati.prototype.fixResult = function(res) {
	if(res.rows.length) {
		return res.rows.item(0);
	} else return {};
}