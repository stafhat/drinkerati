var drinkerati;

document.addEventListener('deviceready', deviceready, false);

function deviceready() {
  
	$('.carousel').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  arrows: false,
	  slidesToShow: 1,
	  adaptiveHeight: true,
	  swipe: true
	});

/*	$('.user-goto').on('click', '.slick-slide', function () {
		$('.carousel').slickGoTo(2);
	});*/

    
    
	drinkerati.getEntries(function(data) {
		var s = "";
		for(var i=0, len=data.length; i<len; i++) {
			s += "<div class='user'><a class='user-goto' href='" + data[i].id + "'><img src='" + data[i].image + "' />" + data[i].name + "</a></div>";
		}
		$("#user-list").html(s);
	});

    $("#addUserSubmit").on("touchstart", function(e) {
		e.preventDefault();
		//grab the values
		var name = $("#entryName").val();
		//store!
		drinkerati.saveEntry({name:name}, function() {
			pageLoad("index.html");
		});
		
	});
    
    
});